import { showModal } from './modal';

export function showWinnerModal(fighter) {
  let winnerInfo = {
    title: 'Winner!',
    bodyElement: fighter.name,
  };

  showModal(winnerInfo);
}
